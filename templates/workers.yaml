{{- $envAll := . }}
{{- range $machine_deployment_name, $machine_deployment_specs := .Values.machine_deployments }}

  {{/*********** Finalize the definition of the machine_deployments.X
  item by merging the machine_deployment_default */}}
  {{- $machine_deployment_def := dict -}}
  {{- $machine_deployment_def := deepCopy ($envAll.Values.machine_deployment_default | default dict) -}}
  {{- $machine_deployment_def := mergeOverwrite $machine_deployment_def $machine_deployment_specs -}}

  {{/*********** Prepare complete labels used in generated object */}}
  {{- $labels := dict -}}
  {{- $labels := deepCopy ($machine_deployment_def.metadata.labels | default dict) -}}
  {{- $_ := mergeOverwrite $labels (include "sylva-capi-cluster.labels" $envAll | fromYaml) -}}

  {{/*********** Set the default machine_deployments.X.infra_provider, if not specifically defined */}}
  {{ if not ($machine_deployment_def.infra_provider) }}
  {{- $_ := set $machine_deployment_def "infra_provider" $envAll.Values.capi_providers.infra_provider -}}
  {{ end }}

  {{/*********** Set the default machine_deployments.X.identity_ref_secret, if not specifically defined for CAPO MD */}}
  {{ if eq $machine_deployment_def.infra_provider "capo" }}
  {{ if not ($machine_deployment_def.identity_ref_secret) }}
  {{- $_ := set $machine_deployment_def "identity_ref_secret" (printf "%s-capo-cloud-config" $envAll.Values.name) -}}
  {{ end }}
  {{ end }}

  {{/*********** Set the default machine_deployments.X.kubelet_extra_args to empty dict, if not specifically defined */}}
  {{ if not ($machine_deployment_def.kubelet_extra_args) }}
  {{- $_ := set $machine_deployment_def "kubelet_extra_args" (dict) -}}
  {{ end }}
---
apiVersion: cluster.x-k8s.io/v1beta1
kind: MachineDeployment
metadata:
  name: {{ $envAll.Values.name }}-worker-md-{{ $machine_deployment_name }}
  namespace: {{ $envAll.Release.Namespace }}
  labels:
    cluster.x-k8s.io/cluster-name: {{ $envAll.Values.name }}
{{ $labels | toYaml | indent 4 }}
  annotations:
{{ $machine_deployment_def.metadata.annotations | toYaml | indent 4 }}
spec:
  clusterName: {{ $envAll.Values.name }}
  replicas: {{ $machine_deployment_def.replicas }}
  selector:
    matchLabels:
      cluster.x-k8s.io/cluster-name: {{ $envAll.Values.name }}
  template:
    metadata:
      labels:
        cluster.x-k8s.io/cluster-name: {{ $envAll.Values.name }}
    spec:
      version: {{ $envAll.Values.k8s_version }}
      clusterName: {{ $envAll.Values.name }}
# TODO: check if at least CAPV (if not CAPD) can use a MachineDeployment.spec.template.spec.failureDomain
# per https://doc.crds.dev/github.com/kubernetes-sigs/cluster-api-provider-vsphere/infrastructure.cluster.x-k8s.io/VSphereFailureDomain/v1beta1@v1.0.2
{{ if eq $machine_deployment_def.infra_provider "capd" }}
{{ else if eq $machine_deployment_def.infra_provider "capo" }}
      failureDomain: {{ $machine_deployment_def.failure_domain }}
{{ else if eq $machine_deployment_def.infra_provider "capv" }}
# TODO: check if CAPM3 does indeed use a MachineDeployment.spec.template.spec.failureDomain
# seems to be still Open in https://github.com/metal3-io/cluster-api-provider-metal3/issues/402
{{ else if eq $machine_deployment_def.infra_provider "capm3" }}
      failureDomain: {{ $machine_deployment_def.failure_domain }}
{{ end }}
      bootstrap:
        configRef:
{{ if eq $envAll.Values.capi_providers.bootstrap_provider "cabpk" }}
          apiVersion: bootstrap.cluster.x-k8s.io/v1beta1
          kind: KubeadmConfigTemplate
          name: {{ $envAll.Values.name }}-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_deployment_def.kubelet_extra_args $machine_deployment_def.additional_commands | include "KubeadmConfigTemplateSpec" | sha1sum | trunc 10 }}
{{ else if eq $envAll.Values.capi_providers.bootstrap_provider "cabpr" }}
          apiVersion: bootstrap.cluster.x-k8s.io/v1alpha1
          kind: RKE2ConfigTemplate
          name: {{ $envAll.Values.name }}-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_deployment_def.kubelet_extra_args $machine_deployment_def.rke2.additionalUserData $machine_deployment_def.additional_commands | include "RKE2ConfigTemplateSpec" | sha1sum | trunc 10 }}
{{ end }}
      infrastructureRef:
{{ if eq $machine_deployment_def.infra_provider "capd" }}
        # https://github.com/kubernetes-sigs/cluster-api/blob/main/test/infrastructure/docker/examples/simple-cluster.yaml
        apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
        kind: DockerMachineTemplate
        name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ include "DockerMachineTemplateSpec" $envAll | sha1sum | trunc 10 }}
{{ else if eq $machine_deployment_def.infra_provider "capo" }}
        apiVersion: infrastructure.cluster.x-k8s.io/v1alpha5
        kind: OpenStackMachineTemplate
        name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_deployment_def.identity_ref_secret $machine_deployment_def.network_interfaces | include "OpenStackMachineTemplateSpec-MD" | sha1sum | trunc 10 }}
{{ else if eq $machine_deployment_def.infra_provider "capv" }}
        apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
        kind: VSphereMachineTemplate
        name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_deployment_def.network_interfaces $machine_deployment_def.profile | include "VSphereMachineTemplateSpec-MD" | sha1sum | trunc 10 }}
{{ else if eq $machine_deployment_def.infra_provider "capm3" }}
        apiVersion: infrastructure.cluster.x-k8s.io/v1alpha5
        kind: Metal3MachineTemplate
        name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_deployment_name $machine_deployment_def.network_interfaces $machine_deployment_def.provisioning_pool_interface $machine_deployment_def.public_pool_interface $machine_deployment_def.capm3.hostSelector | include "Metal3MachineTemplateSpec-MD" | sha1sum | trunc 10 }}
{{ end }}

{{/*********** Implement mixed CP & MD infra providers trick, if the two differ, by creating
another XCluster of the $machine_deployment_def.infra_provider type (DockerCluster/OpenStackCluster/VSphereCluster/Metal3Cluster)
on top of the XCluster of the $envAll.Values.capi_providers.infra_provider type, created in templates/cluster.yaml,
both owned by one common Cluster.cluster.x-k8s.io resource */}}
{{ if not (eq $machine_deployment_def.infra_provider $envAll.Values.capi_providers.infra_provider) }}
---
{{ if eq $machine_deployment_def.infra_provider "capd" }}
apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
kind: DockerCluster
metadata:
  name: {{ $envAll.Values.name }}
  namespace: {{ $envAll.Release.Namespace }}
spec:
  controlPlaneEndpoint:
    host: {{ $envAll.Values.cluster_external_ip }}
    port: 6443
{{ else if eq $machine_deployment_def.infra_provider "capo" }}
apiVersion: infrastructure.cluster.x-k8s.io/v1alpha5
kind: OpenStackCluster
metadata:
  name: {{ $envAll.Values.name }}
  namespace: {{ $envAll.Release.Namespace }}
spec:
  cloudName: capo_cloud
  externalNetworkId: {{ $envAll.Values.capo.network_id }}
  network:
    id: {{ $envAll.Values.capo.network_id }}
  apiServerFixedIP: {{ $envAll.Values.cluster_external_ip }}
  identityRef:
    kind: Secret
    name: {{ $machine_deployment_def.identity_ref_secret }}
  managedSecurityGroups: true
  disableAPIServerFloatingIP: true
  allowAllInClusterTraffic: true
  tags:
  - {{ $envAll.Values.capo.resources_tag }}
{{ if $envAll.Values.capo.control_plane_az }}
  controlPlaneAvailabilityZones: {{ $envAll.Values.capo.control_plane_az | toJson }}
{{ end }}
{{ else if eq $machine_deployment_def.infra_provider "capv" }}
apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
kind: VSphereCluster
metadata:
  name: {{ $envAll.Values.name }}
  namespace: {{ $envAll.Release.Namespace }}
spec:
  controlPlaneEndpoint:
    host: {{ $envAll.Values.cluster_external_ip }}
    port: 6443
  identityRef:
    kind: Secret
    name: {{ $envAll.Values.name }}
  server: {{ $envAll.Values.capv.server }}
  thumbprint: {{ $envAll.Values.capv.tlsThumbprint }}
{{ else if eq $machine_deployment_def.infra_provider "capm3" }}
apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
kind: Metal3Cluster
metadata:
  name: {{ $envAll.Values.name }}
  namespace: {{ $envAll.Release.Namespace }}
spec:
  controlPlaneEndpoint:
    host: {{ $envAll.Values.cluster_external_ip }}
    port: 6443
  noCloudProvider: true
{{ end }}
{{ end }}
{{- end }}
