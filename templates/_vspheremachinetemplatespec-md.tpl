{{ define "VSphereMachineTemplateSpec-MD" }}
{{- $envAll := index . 0 -}}
{{- $machine_network_interfaces := index . 1 -}}
{{- $profile := index . 2 -}}
cloneMode: {{ $envAll.Values.capv.template_clone_mode }}
datacenter: {{ $envAll.Values.capv.dataCenter | quote }}
{{- if $envAll.Values.capv.dataStore }}
datastore: {{ $envAll.Values.capv.dataStore }}
{{- end }}
folder: {{ $envAll.Values.capv.folder }}
resourcePool: {{ $envAll.Values.capv.resourcePool }}
server: {{ $envAll.Values.capv.server }}
{{- if $envAll.Values.capv.storagePolicyName }}
storagePolicyName: {{ $envAll.Values.capv.storagePolicyName }}
{{- end }}
template: {{ $envAll.Values.image }}
thumbprint: {{ $envAll.Values.capv.tlsThumbprint }}
diskGiB: {{ $profile.diskGiB }}
memoryMiB: {{ $profile.memoryMiB }}
numCPUs: {{ $profile.numCPUs }}
network:
  devices:
  {{- range $common_network_interface_name, $common_network_interface_def := $envAll.Values.capv.networks }}
  {{- if $common_network_interface_def.networkName }}
    - dhcp4: {{ $common_network_interface_def.dhcp4 | default false }}
      networkName: {{ $common_network_interface_def.networkName }}
  {{- end }}
  {{- end }}
  {{- range $machine_network_interface_name, $machine_network_interface_def := $machine_network_interfaces }}
  {{- if $machine_network_interface_def.networkName }}
    - dhcp4: {{ $machine_network_interface_def.dhcp4 | default false }}
      networkName: {{ $machine_network_interface_def.networkName }}
  {{- end }}
  {{- end }}
{{ end }}
