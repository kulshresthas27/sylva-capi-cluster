{{- define "DockerMachineTemplateSpec" }}
{{- if and .Values.image (eq .Values.capi_providers.bootstrap_provider "cabpr") }}
customImage: {{ .Values.image }}
{{- end }}
{{- if .Values.capd.docker_host_socket }}
extraMounts:
  - containerPath: {{ .Values.capd.docker_host_socket }}
    hostPath: {{ .Values.capd.docker_host_socket }}
{{- end }}
{{- end }}
