{{- define "KubeadmConfigTemplateSpec" -}}
{{- $envAll := index . 0 -}}
{{- $machine_kubelet_extra_args := index . 1 -}}
{{- $machine_additional_commands := index . 2 -}}

  {{/*********** Initialize the components of the KubeadmConfigTemplate.spec.template.spec fields */}}
  {{- $base := tuple $envAll $machine_kubelet_extra_args | include "base-KubeadmConfigTemplateSpec" | fromYaml }}
  {{- $infra := include (printf "%s-KubeadmConfigTemplateSpec" $envAll.Values.capi_providers.infra_provider) $envAll | fromYaml }}

joinConfiguration: {{ mergeOverwrite $base.joinConfiguration $infra.joinConfiguration | toYaml | nindent 2 }}
{{- if $envAll.Values.ntp }}
ntp: {{ mergeOverwrite $base.ntp $infra.ntp | toYaml | nindent 2 }}
{{- end }}
preKubeadmCommands:
  {{ $infra.preKubeadmCommands | toYaml | nindent 2 }}
  {{ $base.preKubeadmCommands | toYaml | nindent 2 }}
{{- if $machine_additional_commands.pre_bootstrap_commands }}
  {{ $machine_additional_commands.pre_bootstrap_commands | toYaml | nindent 2 }}
{{- end }}
files: {{ concat $base.files $infra.files | toYaml | nindent 2 }}
users: {{ concat $base.users $infra.users | toYaml | nindent 2 }}
postKubeadmCommands:
{{- if $machine_additional_commands.post_bootstrap_commands }}
  {{ $machine_additional_commands.post_bootstrap_commands | toYaml | nindent 2 }}
{{- else }}
  []
{{- end }}
{{- end }}
